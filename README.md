**Pour compiler** :

  `tsc src/test.ts --outDir build`

**Pour exécuter** :

  `node build/test.js`