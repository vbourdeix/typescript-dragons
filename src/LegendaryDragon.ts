import { Dragon } from './Dragon';
import { Random } from '../services/Random';

export class LegendaryDragon extends Dragon {
    private defenseBonus: number;
    private attackBonus: number;

    constructor(name) {
        super(name);
        this.defenseBonus = Random.getIntegerBetweenValues(3, 11);
        this.attackBonus = Random.getIntegerBetweenValues(4, 12);
    }

    saluer() {
        super.saluer();
        console.log('Et je suis légendaire !');
    }

    receiveDamages(damages: number) {
        damages = Math.max(0, damages - this.defenseBonus);
        super.receiveDamages(damages);
    }

    getRandomDamageValue(): number {
        const damages = super.getRandomDamageValue();
        return damages + this.attackBonus;
    }
}
