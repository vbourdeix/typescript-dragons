import { DragonColor } from './DragonColor';
import { DragonPower } from './DragonPower';
import { Random } from './Random';

const DragonColorsNumber = Object.keys(DragonColor).length / 2;

export class Dragon {
    private name: string;
    private size: number;
    private color: DragonColor;
    private power: DragonPower;
    private lifePoints: number;

    constructor(name, lifePoints = 100) {
        this.name = name;
        this.lifePoints = lifePoints;
        this.setRandomSize();
        this.setRandomColor();
        this.setRandomPower();
    }

    public getColor(): DragonColor {
        return this.color;
    }

    public getPower(): DragonPower {
        return this.power;
    }

    public getSize(): number {
        return this.size;
    }

    public getName(): string {
        return this.name;
    }

    private setRandomSize(): void {
        this.size = Random.getIntegerBetweenValues(1, 100);
    }

    private setRandomColor(): void {
        this.color = Random.getEnumValue(DragonColor);
    }

    private setRandomPower(): void {
        this.power = Random.getEnumValue(DragonPower);
    }

    saluer():void {
        console.log(`Bonjour je m'appelle ${this.name}, je mesure ${this.size}m et je suis couleur ${this.color}`);
    }

    receiveDamages(damages: number)
    {
        this.lifePoints = Math.max(0, this.lifePoints - damages);
        if (this.lifePoints === 0) {
            console.log(`
                AAARRRRGGGHHHHH !!!
                ${this.name} a été tué au combat
            `);
        }
    }

    attack(targetDragon: Dragon) {
        const strength = this.getRandomDamageValue();
        console.log(`${this.name} fait une attaque de ${this.power} de puissance ${strength} sur ${targetDragon.name}`);
        targetDragon.receiveDamages(strength);
    }

    getRandomDamageValue(): number {
        let damages = 0;
        // Un dragon de grande taille doit avoir plus de chances d'infliger de gros dégats qu'un tout petit dragon
        const trialsNumber = Math.max(1, Math.round(this.size / 25));
        for (let i = 1; i <= trialsNumber; i++) {
            damages = Random.getIntegerBetweenValues(1, 12);
        }
        return damages;
    }

    isAlive(): boolean {
        return this.lifePoints > 0;
    }

    isDead(): boolean {
        return !this.isAlive();
    }

    displayCharacteristics() {
        console.log(`
        Le dragon ${this.name} est un dragon couleur ${this.color}, fait ${this.size} mètres de haut et maîtrise le pouvoir ${this.power}
        Il lui reste ${this.lifePoints} points de vie
        `);
    }
}
