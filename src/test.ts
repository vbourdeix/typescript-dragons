import { Dragon } from './Dragon';
import { LegendaryDragon } from './LegendaryDragon';
import { Tournament } from './Tournament';

const voldemort = new LegendaryDragon('Voldemort');
const draco = new Dragon('Draco');
const coeurDeGivre = new Dragon('Coeur de Givre');
const shenron = new LegendaryDragon('Shenron');
const acnologia = new Dragon('Acnologia');
const spyro = new Dragon('Spyro');

const tournament = new Tournament();
tournament.registerDragons(
    voldemort, draco, coeurDeGivre, shenron, acnologia, spyro
);
tournament.start();