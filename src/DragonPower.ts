export enum DragonPower {
    FIRE,
    ICE,
    WIND,
    EARTH,
    GRAVITY,
    VOID,
    WATER,
    THUNDER,
    ARCANA
}
