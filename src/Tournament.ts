import { Dragon } from './Dragon';
import { Random } from './Random';

export class Tournament {
    constructor (private finished:boolean = false, private registeredDragons: Array<Dragon> = []) {
    }

    registerDragons(...dragons: Array<Dragon>) {
        for (let dragon of dragons) {
            this.registeredDragons.push(dragon);
        }
    }

    start() {
        if (this.registeredDragons.length < 2) {
            console.log("Pas assez de dragons pour démarrer un tournoi");
            return;
        }
        this.displayCharacteristics();

        let aDragonDiedFighting = false;
        while (aDragonDiedFighting == false) {
            let randomDragonIndex = Random.getIntegerBetweenValues(0, this.registeredDragons.length - 1);
            let attackingDragon = this.registeredDragons[randomDragonIndex];
            let targetDragon = attackingDragon;
        
            // Détermination aléatoire d'une cible. On recommence si la cible est identique à l'attaquant
            do {
                let randomDragonIndex = Random.getIntegerBetweenValues(0, this.registeredDragons.length - 1);
                targetDragon = this.registeredDragons[randomDragonIndex];
            } while (attackingDragon.getName() == targetDragon.getName());
        
            attackingDragon.attack(targetDragon);
            if (targetDragon.isDead()) {
                aDragonDiedFighting = true;
            }
        }

        console.log("Malédiction ! Les dragons se sont battus avec trop d'intensité. L'un d'entre eux est mort et par conséquent, nous devons arrêter ce tournoi immédiatement !");
    }

    displayCharacteristics() {
        if (this.finished) {
            console.log("Ce tournoi est déjà terminé")
        } else {
            console.log(`Voici les dragons inscrits au tournoi :`);
            for (let dragon of this.registeredDragons) {
                dragon.displayCharacteristics();
            }
        }
    }
}