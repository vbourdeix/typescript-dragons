export enum DragonColor {
    SAPPHIRE,
    EMERALD,
    RUBY,
    TOPAZ,
    DIAMOND,
    GOLD,
    SILVER,
    COPPER,
    TIN,
    JADE
}
