"use strict";
exports.__esModule = true;
var Random_1 = require("./Random");
var Tournament = /** @class */ (function () {
    function Tournament(finished, registeredDragons) {
        if (finished === void 0) { finished = false; }
        if (registeredDragons === void 0) { registeredDragons = []; }
        this.finished = finished;
        this.registeredDragons = registeredDragons;
    }
    Tournament.prototype.registerDragons = function () {
        var dragons = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            dragons[_i] = arguments[_i];
        }
        for (var _a = 0, dragons_1 = dragons; _a < dragons_1.length; _a++) {
            var dragon = dragons_1[_a];
            this.registeredDragons.push(dragon);
        }
    };
    Tournament.prototype.start = function () {
        if (this.registeredDragons.length < 2) {
            console.log("Pas assez de dragons pour démarrer un tournoi");
            return;
        }
        this.displayCharacteristics();
        var aDragonDiedFighting = false;
        while (aDragonDiedFighting == false) {
            var randomDragonIndex = Random_1.Random.getIntegerBetweenValues(0, this.registeredDragons.length - 1);
            var attackingDragon = this.registeredDragons[randomDragonIndex];
            var targetDragon = attackingDragon;
            // Détermination aléatoire d'une cible. On recommence si la cible est identique à l'attaquant
            do {
                var randomDragonIndex_1 = Random_1.Random.getIntegerBetweenValues(0, this.registeredDragons.length - 1);
                targetDragon = this.registeredDragons[randomDragonIndex_1];
            } while (attackingDragon.getName() == targetDragon.getName());
            attackingDragon.attack(targetDragon);
            if (targetDragon.isDead()) {
                aDragonDiedFighting = true;
            }
        }
        console.log("Malédiction ! Les dragons se sont battus avec trop d'intensité. L'un d'entre eux est mort et par conséquent, nous devons arrêter ce tournoi immédiatement !");
    };
    Tournament.prototype.displayCharacteristics = function () {
        if (this.finished) {
            console.log("Ce tournoi est déjà terminé");
        }
        else {
            console.log("Voici les dragons inscrits au tournoi :");
            for (var _i = 0, _a = this.registeredDragons; _i < _a.length; _i++) {
                var dragon = _a[_i];
                dragon.displayCharacteristics();
            }
        }
    };
    return Tournament;
}());
exports.Tournament = Tournament;
