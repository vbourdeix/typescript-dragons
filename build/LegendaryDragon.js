"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var Dragon_1 = require("./Dragon");
var Random_1 = require("./Random");
var LegendaryDragon = /** @class */ (function (_super) {
    __extends(LegendaryDragon, _super);
    function LegendaryDragon(name) {
        var _this = _super.call(this, name) || this;
        _this.defenseBonus = Random_1.Random.getIntegerBetweenValues(3, 11);
        _this.attackBonus = Random_1.Random.getIntegerBetweenValues(4, 12);
        return _this;
    }
    LegendaryDragon.prototype.saluer = function () {
        _super.prototype.saluer.call(this);
        console.log('Et je suis légendaire !');
    };
    LegendaryDragon.prototype.receiveDamages = function (damages) {
        damages = Math.max(0, damages - this.defenseBonus);
        _super.prototype.receiveDamages.call(this, damages);
    };
    LegendaryDragon.prototype.getRandomDamageValue = function () {
        var damages = _super.prototype.getRandomDamageValue.call(this);
        return damages + this.attackBonus;
    };
    return LegendaryDragon;
}(Dragon_1.Dragon));
exports.LegendaryDragon = LegendaryDragon;
