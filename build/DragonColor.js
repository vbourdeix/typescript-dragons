"use strict";
exports.__esModule = true;
var DragonColor;
(function (DragonColor) {
    DragonColor[DragonColor["SAPPHIRE"] = 0] = "SAPPHIRE";
    DragonColor[DragonColor["EMERALD"] = 1] = "EMERALD";
    DragonColor[DragonColor["RUBY"] = 2] = "RUBY";
    DragonColor[DragonColor["TOPAZ"] = 3] = "TOPAZ";
    DragonColor[DragonColor["DIAMOND"] = 4] = "DIAMOND";
    DragonColor[DragonColor["GOLD"] = 5] = "GOLD";
    DragonColor[DragonColor["SILVER"] = 6] = "SILVER";
    DragonColor[DragonColor["COPPER"] = 7] = "COPPER";
    DragonColor[DragonColor["TIN"] = 8] = "TIN";
    DragonColor[DragonColor["JADE"] = 9] = "JADE";
})(DragonColor = exports.DragonColor || (exports.DragonColor = {}));
