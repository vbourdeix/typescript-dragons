"use strict";
exports.__esModule = true;
var DragonColor_1 = require("./DragonColor");
var DragonPower_1 = require("./DragonPower");
var Random_1 = require("./Random");
var DragonColorsNumber = Object.keys(DragonColor_1.DragonColor).length / 2;
var Dragon = /** @class */ (function () {
    function Dragon(name, lifePoints) {
        if (lifePoints === void 0) { lifePoints = 100; }
        this.name = name;
        this.lifePoints = lifePoints;
        this.setRandomSize();
        this.setRandomColor();
        this.setRandomPower();
    }
    Dragon.prototype.getColor = function () {
        return this.color;
    };
    Dragon.prototype.getPower = function () {
        return this.power;
    };
    Dragon.prototype.getSize = function () {
        return this.size;
    };
    Dragon.prototype.getName = function () {
        return this.name;
    };
    Dragon.prototype.setRandomSize = function () {
        this.size = Random_1.Random.getIntegerBetweenValues(1, 100);
    };
    Dragon.prototype.setRandomColor = function () {
        this.color = Random_1.Random.getEnumValue(DragonColor_1.DragonColor);
    };
    Dragon.prototype.setRandomPower = function () {
        this.power = Random_1.Random.getEnumValue(DragonPower_1.DragonPower);
    };
    Dragon.prototype.saluer = function () {
        console.log("Bonjour je m'appelle " + this.name + ", je mesure " + this.size + "m et je suis couleur " + this.color);
    };
    Dragon.prototype.receiveDamages = function (damages) {
        this.lifePoints = Math.max(0, this.lifePoints - damages);
        if (this.lifePoints == 0) {
            console.log("\n                AAARRRRGGGHHHHH !!!\n                " + this.name + " a \u00E9t\u00E9 tu\u00E9 au combat\n            ");
        }
    };
    Dragon.prototype.attack = function (targetDragon) {
        var strength = this.getRandomDamageValue();
        console.log(this.name + " fait une attaque de " + this.power + " de puissance " + strength + " sur " + targetDragon.name);
        targetDragon.receiveDamages(strength);
    };
    Dragon.prototype.getRandomDamageValue = function () {
        var damages = 0;
        // Un dragon de grande taille doit avoir plus de chances d'infliger de gros dégats qu'un tout petit dragon
        var trialsNumber = Math.max(1, Math.round(this.size / 25));
        for (var i = 1; i <= trialsNumber; i++) {
            damages = Random_1.Random.getIntegerBetweenValues(1, 12);
        }
        return damages;
    };
    Dragon.prototype.isAlive = function () {
        return this.lifePoints > 0;
    };
    Dragon.prototype.isDead = function () {
        return !this.isAlive();
    };
    Dragon.prototype.displayCharacteristics = function () {
        console.log("\n        Le dragon " + this.name + " est un dragon couleur " + this.color + ", fait " + this.size + " m\u00E8tres de haut et ma\u00EEtrise le pouvoir " + this.power + "\n        Il lui reste " + this.lifePoints + " points de vie\n        ");
    };
    return Dragon;
}());
exports.Dragon = Dragon;
