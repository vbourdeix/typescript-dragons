"use strict";
exports.__esModule = true;
var DragonPower;
(function (DragonPower) {
    DragonPower[DragonPower["FIRE"] = 0] = "FIRE";
    DragonPower[DragonPower["ICE"] = 1] = "ICE";
    DragonPower[DragonPower["WIND"] = 2] = "WIND";
    DragonPower[DragonPower["EARTH"] = 3] = "EARTH";
    DragonPower[DragonPower["GRAVITY"] = 4] = "GRAVITY";
    DragonPower[DragonPower["VOID"] = 5] = "VOID";
    DragonPower[DragonPower["WATER"] = 6] = "WATER";
    DragonPower[DragonPower["THUNDER"] = 7] = "THUNDER";
    DragonPower[DragonPower["ARCANA"] = 8] = "ARCANA";
})(DragonPower = exports.DragonPower || (exports.DragonPower = {}));
