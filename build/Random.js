"use strict";
exports.__esModule = true;
var Random = /** @class */ (function () {
    function Random() {
    }
    // Hack : https://stackoverflow.com/questions/44230998/how-to-get-a-random-enum-in-typescript
    Random.getEnumValue = function (enumType) {
        var enumSize = Object.keys(enumType).length / 2;
        var itemNumericalIndex = Random.getIntegerBetweenValues(0, enumSize - 1);
        var itemIndex = Object.keys(enumType)[itemNumericalIndex];
        return enumType[itemIndex];
    };
    Random.getIntegerBetweenValues = function (min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    };
    return Random;
}());
exports.Random = Random;
